Mapa 1

```plantuml
@startmindmap
*[#lightGoldenRodYellow] **Consultoria**
 *[#lightBlue]  **Nace**
  *_ De
   *[#lightBlue]  **La existencia de** \n **grandes organizaciones**
    *_ Con
     *[#lightBlue]  **gran complejidad interna**
      *_ Requiriendo
       *[#lightBlue]  **sistemas de informacion**
        *_ Que 
         *[#lightBlue]  **Den soporte a**
          *_ Requiriendo
           *[#lightBlue]  **Sistemas  complejos**
            *_ Manteniedno
             *[#lightBlue]  **Una actualizacion constante en dicha empresa**
    *_ Ejemplo
     *[#lightBlue]  **Axpe Consulting**         
 *[#lightBlue]  **Servicio**\n**que presta**


  *[#lightBlue]  **Consultoria**
   *_ Cuenta con
    *[#lightBlue]  **Ingenieria de procesos**
    *[#lightBlue]  **Gobierno TI**
    *[#lightBlue]  **Oficina de proyectos**
    *[#lightBlue]  **Analitica avanzada de datos**
    *_ Son de tipo

     *[#lightBlue]  **Social**
      *[#lightBlue]  **Sentimental analitic**
      *[#lightBlue]  **Mejoramiento de atencion por redes sociales**
     *[#lightBlue]  **Movilidad**
      *[#lightBlue]  **Pacticas de Gobierno TI**
       *_ Para
        *[#lightBlue]  **Mobile**
      *[#lightBlue]  **Simplificacion de procesos**
       *_ Con
        *[#lightBlue]  **Mobilidad**  
     *[#lightBlue]  **Analitica**
      *[#lightBlue] **Definicion de arquitectura**
       *[#lightBlue] **BIGDATA**
      *[#lightBlue] **Desarrollo**
       *_ De
        *[#lightBlue] **Modelos analiticos avanzados**
     *[#lightBlue]  **Nube**
      *[#lightBlue] **Definicion**
       *_ De
        *[#lightBlue] **Estrategia**
        *[#lightBlue] **Procedimientos de operacion**

  *[#lightBlue]  **Integracion**
   *_ Cuenta con
    *[#lightBlue]  **Desarrllos a medida**
    *[#lightBlue]  **Aseguramiento de calidad**
    *[#lightBlue]  **Infraestructura**
    *[#lightBlue]  **Soluciones de mercado**
    *_ Son de tipo

     *[#lightBlue]  **Social**
      *[#lightBlue]  **Desarrollo de conecciones pro Redes Sociales**
     *[#lightBlue] **Movilidad**
      *[#lightBlue] **Movilizacion de aplicaciones**
       *_ Ya existentes
        *[#lightBlue] **Bootstrap**
        *[#lightBlue] **Cordova**
      *[#lightBlue] **Desarrollo y prueba de apps **
      *[#lightBlue] **Integracion de soluciones**
       *_ Como
        *[#lightBlue] **Firma Biomertrica**
        *[#lightBlue] **SMS/Mail certificados**  
     *[#lightBlue]  **Analitica**
      *[#lightBlue] **Integracion de fuentes**
      *[#lightBlue] **desarrollo de Cdm**
      *[#lightBlue] **Integracion de entornos**
       *_ De tipo
        *[#lightBlue] **Relacionales y BIGDATA**
     *[#lightBlue]  **Nube**
      *[#lightBlue] **Implementacion de estrategias**
      *[#lightBlue] **Desarrollo de herramientas**
       *_ De
        *[#lightBlue] **Provision**
      *[#lightBlue] **Desarrollo de aplicaciones**  

  *[#lightBlue]  **Externalizacion**
   *_ Cuenta con
    *[#lightBlue]  **Gestion de aplicaciones**
    *[#lightBlue]  **Servicios SQA**
    *[#lightBlue]  **Operacion y administracion**
    *[#lightBlue]  **Procesos de negocio**
    *_ Son de tipo
     *[#lightBlue]  **Social**
      *[#lightBlue]  **Comunity Manager**
     *[#lightBlue]  **Movilidad**
      *[#lightBlue] **Operacion de plataformas**
       *_ De
        *[#lightBlue] **De aplicaciones moviles**
     *[#lightBlue]  **Analitica**
      *[#lightBlue] **Factoria de explotacion**
       *_ Y mantenimiento
        *[#lightBlue] **De modelos analiticos**
     *[#lightBlue]  **Nube**
      *[#lightBlue] **Operacion de servicios**

 *[#lightBlue]  **A futuro:**
  *[#lightBlue] **El conocimiento**
   *_ De
    *[#lightBlue] **Consultoria**
     *_ Permite 
      *[#lightBlue] **Alcanzar la felicidad profesional**
 *[#lightBlue]  **¿Que exige?**
  *_ Caracteristicas
   *[#lightBlue] **Actitud**
   *[#lightBlue] **Aptitud**
   *[#lightBlue] **Voluntad de mejora**
   *[#lightBlue] **Compromiso**
 *[#lightBlue]  **¿Como es en el ambito profesional ?**
  *[#lightBlue] **Segun**
   *_ Por su
    *[#lightBlue] **Nivel**
     *[#lightBlue] **Junior**
     *[#lightBlue] **Senior**
     *[#lightBlue] **Gerente**
     *[#lightBlue] **Firector**
@endmindmap
```mindmap
```

Mapa 2

```plantuml
@startmindmap
*[#lightGoldenRodYellow] **Consultoria** \n **De Software**
 *[#lightBlue]  **Diseño de software**
  *_ Nace a partir
   *[#lightBlue] **De la necesidad del cliente**
  *[#lightBlue] **Etapas** 
   *[#lightBlue] **Consultoria**
    *[#lightBlue] **Necesidades que tiene**
   *[#lightBlue] **Estudio de viabilidad**
    *[#lightBlue] **Lo que el cliente  quiere**
   *[#lightBlue] **Diseño Funcional**
    *[#lightBlue] **Prototipo**
     *_ Basados en
      *[#lightBlue] **Modelos de datos**
      *[#lightBlue] **Informacion**
       *_ De
        *[#lightBlue] **Entrada**
        *[#lightBlue] **Salida**
   *[#lightBlue] **Diseño Tecnico** 
    *_ Se plasma 
     *[#lightBlue] **A nivel Hardware**
      *_ Usando  
       *[#lightBlue] **Lenguajes de programacion**
        *_ El
         *[#lightBlue] **Modelo Funcional**
   *[#lightBlue] **Puebas**
    *[#lightBlue] **De software**
    *[#lightBlue] **De Hardware**
 *[#lightBlue]  **Consultoria informatica**
  *_ Tareas que desarrolla
   *[#lightBlue]  **Tareas**
    *_ Que ayudan
     *[#lightBlue]  **Al desarrollo informatico de dicha empresa**
      *_ Mediante
       *[#lightBlue]  **Software**
        *_ Tiempo de vida 
         *[#lightBlue]  **Depende del volumen de la empresa**
   *_ Dan en mayor parte 
    *[#lightBlue]  **Servicio solo a empresas**
 *[#lightBlue]  **Tendencias en el desarrollo de servicios**
  *[#lightBlue]  **Infrestructura Empresarial:**
   *_ Como
    *[#lightBlue]  **Almacenamiento en la nube**
     *[#lightBlue]  **Recabacion de grandes datos**
 *[#lightBlue]  **Almacenamiento** 
  *[#lightBlue]  **Se exige una mayor cantidad de espacio**
   *_ Como
    *[#lightBlue]  **BIGDATA**
 *[#lightBlue]  **Metodologias de desarrollo de proyecto**
  *[#lightBlue]  **Agiles**  
   *_ Evita  
    *[#lightBlue]  **La separacion de etapas en el desarrollo**
     *_ Del
      *[#lightBlue]  **Proyecto**
 *[#lightBlue]  **Desventajas**
  *[#lightBlue]  **Filtracion de informacion a Terceros**
  *[#lightBlue]  **Restricciones de LEY dependiendo del**\n** area geografica y de almacenamiento**\n** seleccionados**
  *[#lightBlue]  **Inmadurez del cliente**
@endmindmap
```mindmap
```




Mapa 3

```plantuml
@startmindmap
*[#lightBlue]  **Aplicación de la **\n**ingeniería de software**
 *[#lightBlue]  **Empresas**
  *_ Necesidad de
   *[#lightBlue]  **Productos comerciales**
   *[#lightBlue]  **Soluciones ERP**
    *_ Pretende cubrir
     *[#lightBlue]  **Todos los procesos de negocio**
      *_ Dentro de
       *[#lightBlue]  **Una empresa**
  *_ De ayuda
   *[#lightBlue]  **SAP**
    *[#lightBlue]  **Empresa resaltante en el campo **
     *_ De creacion
      *[#lightBlue]  **De software de gestion**
   *_ Se necesita
    *[#lightBlue]  **Su manipulacion**
    *[#lightBlue]  **Etapas**
     *[#lightBlue]  **Parametrizacion** 
      *[#lightBlue]  **Definicion del proceso del negocio**
     *[#lightBlue]  **Frame de dasarrollo**
      *[#lightBlue]  **Para cosas particulares**
      *[#lightBlue]  **Se manipula en**
       *[#lightBlue]  **JAVA**
  *[#lightBlue]  **Siempre se comienza por la**\n** metodologia estandar antes vista**

@endmindmap
```mindmap
```